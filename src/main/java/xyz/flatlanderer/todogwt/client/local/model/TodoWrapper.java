package xyz.flatlanderer.todogwt.client.local.model;

import javax.validation.constraints.NotNull;

public class TodoWrapper {
    private Integer index;
    @NotNull
    private Todo todo;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }
}
