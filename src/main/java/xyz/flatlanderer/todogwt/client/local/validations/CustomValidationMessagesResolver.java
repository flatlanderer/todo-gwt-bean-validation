package xyz.flatlanderer.todogwt.client.local.validations;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.ConstantsWithLookup;
import com.google.gwt.validation.client.AbstractValidationMessageResolver;
import com.google.gwt.validation.client.UserValidationMessagesResolver;

public class CustomValidationMessagesResolver extends AbstractValidationMessageResolver implements UserValidationMessagesResolver {
    protected CustomValidationMessagesResolver() {
        super((ConstantsWithLookup) GWT.create(ValidationMessages.class));
    }
}
