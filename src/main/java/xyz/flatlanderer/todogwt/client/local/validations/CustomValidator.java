package xyz.flatlanderer.todogwt.client.local.validations;

import org.jboss.errai.common.client.logging.util.Console;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomValidator {
    private static Logger logger = Logger.getLogger(CustomValidator.class.getName());
    private static Validator validator;

    public static void init() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    public static <T extends Object> Set<ConstraintViolation<T>> validate(T bean) {
        return validate(bean, null);
    }

    public static <T extends Object> Set<ConstraintViolation<T>> validate(T bean, Class... sequences) {
        init();
        Set<ConstraintViolation<T>> violations = null;
        try {
            if (sequences != null) {
                violations = validator.validate(bean, sequences);
            } else {
                Console.log("Validating...");
                violations = validator.validate(bean);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error trying to perform validations for bean: " + bean.getClass().getName(), e);
        }
        return violations;
    }
}
