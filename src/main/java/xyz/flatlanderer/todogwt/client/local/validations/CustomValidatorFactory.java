package xyz.flatlanderer.todogwt.client.local.validations;

import com.google.gwt.core.client.GWT;
import com.google.gwt.validation.client.AbstractGwtValidatorFactory;
import com.google.gwt.validation.client.GwtValidation;
import com.google.gwt.validation.client.impl.AbstractGwtValidator;
import xyz.flatlanderer.todogwt.client.local.model.Todo;
import xyz.flatlanderer.todogwt.client.local.model.TodoWrapper;
import xyz.flatlanderer.todogwt.client.local.validations.groups.DraftValidationGroup;
import xyz.flatlanderer.todogwt.client.local.validations.groups.PersistenceValidationGroup;

import javax.validation.Validator;

public final class CustomValidatorFactory extends AbstractGwtValidatorFactory {

    @Override
    public AbstractGwtValidator createValidator() {
        return GWT.create(GwtValidator.class);
    }

    @GwtValidation(value = {Todo.class, TodoWrapper.class},
            groups = {DraftValidationGroup.class, PersistenceValidationGroup.class})
    public interface GwtValidator extends Validator {
    }

}
