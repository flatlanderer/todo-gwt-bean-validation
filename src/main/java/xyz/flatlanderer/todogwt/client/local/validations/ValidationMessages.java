package xyz.flatlanderer.todogwt.client.local.validations;

public interface ValidationMessages extends org.hibernate.validator.ValidationMessages {
    @DefaultStringValue("Enter a valid email address")
    @Key("invalid.email")
    String invalidEmailMessage();

}
