package xyz.flatlanderer.todogwt.client.local.validations.constraints;

import xyz.flatlanderer.todogwt.client.local.validations.validators.NotEmptyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NotEmptyValidator.class)
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@NotNull
public @interface NotEmpty {
    String message() default "Field should not be empty";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
