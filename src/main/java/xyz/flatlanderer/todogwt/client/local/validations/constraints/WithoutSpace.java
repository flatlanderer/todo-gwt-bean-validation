package xyz.flatlanderer.todogwt.client.local.validations.constraints;
import xyz.flatlanderer.todogwt.client.local.validations.validators.WithoutSpaceValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = WithoutSpaceValidator.class)
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface WithoutSpace {
    String message() default "Field should not have any space";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
