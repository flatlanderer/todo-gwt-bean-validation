package xyz.flatlanderer.todogwt.client.local.validations.groups;

import javax.validation.groups.Default;

public interface DraftValidationGroup extends Default {
}
