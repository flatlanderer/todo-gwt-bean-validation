package xyz.flatlanderer.todogwt.client.local.validations.validators;

import xyz.flatlanderer.todogwt.client.local.validations.constraints.NotEmpty;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotEmptyValidator implements ConstraintValidator<NotEmpty, String> {

    public void initialize(NotEmpty constraintAnnotation) {
    }

    public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
        return object != null && !object.trim().isEmpty();
    }

}