package xyz.flatlanderer.todogwt.client.local.validations.validators;

import xyz.flatlanderer.todogwt.client.local.validations.constraints.WithoutSpace;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class WithoutSpaceValidator implements ConstraintValidator<WithoutSpace, String> {

    public void initialize(WithoutSpace constraintAnnotation) {
    }

    public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
        boolean hasWhiteSpace = false;
        if (object != null) {
            for (char c : object.toCharArray()) {
                if (Character.isWhitespace(c)) {
                    hasWhiteSpace = true;
                }
            }
        }
        return object != null && !hasWhiteSpace;
    }

}